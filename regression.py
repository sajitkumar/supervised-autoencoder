from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split
from sklearn.metrics import median_absolute_error, r2_score
from supervisedAE import SupervisedAE
from sklearn.preprocessing import MinMaxScaler
import torch

if __name__ == "__main__":

    X, y = make_regression(n_samples=10000, noise=100,n_targets=1, random_state=0)

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)

    sae = SupervisedAE(input_dim=100, emb_dim=10, num_targets=1, num_chunks=100, num_layers=1,activation='relu',
                    learning_rate=0.001,weight_decay=1e-5,convg_thres=1e-5,max_epochs=500,
                    supervision='reg',is_last_linear=False)
    sae.fit(X_train, y_train)
    y_pred, _ = sae.predict(X_test, 10)
    y_pred = y_pred.detach()
    print("R2 Score: {%.2f}",
        r2_score(y_test, y_pred))