from sklearn import datasets, metrics
from sklearn.model_selection import train_test_split
import torch
from supervisedAE import SupervisedAE


if __name__ == "__main__":
        # The digits dataset
    digits = datasets.load_digits()

    n_samples = len(digits.images)
    data = digits.images.reshape((n_samples, -1))

    # Split data into train and test subsets
    X_train, X_test, y_train, y_test = train_test_split(
        data, digits.target, test_size=0.25, shuffle=False)

    # We learn the digits on the first half of the digits
    sae = SupervisedAE(64, 16, 10, 10, 2, activation='relu',
                    learning_rate=0.0003676378455085538,weight_decay=9.561184465832839e-05,convg_thres=1e-5,max_epochs=1000,
                    supervision='clf',is_last_linear=False)
    sae.fit(X_train, y_train)
    # Now predict the value of the digit on the second half:
    predicted, _ = sae.predict(X_test, 5)
    y_pred = torch.argmax(predicted, 1).numpy()
    print("Classification report for sae:\n%s\n"
        % (metrics.classification_report(y_test, y_pred)))