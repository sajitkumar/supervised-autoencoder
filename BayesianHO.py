from skopt.utils import use_named_args
from supervisedAE import SupervisedAE
import pandas as pd
from sklearn import metrics
from sklearn.model_selection import train_test_split
import torch
from skopt import gp_minimize
from skopt.space import Integer, Real, Categorical
##Load dataset
...............
X=
y=
# Split data into train and test subsets
X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.25, shuffle=False)

#path to save model file
path = 'model.pt'

#Input parameter for Autoencoders
input_dim = 64
emb_dim = 16
num_targets = 10
num_chunks = 10
supervision = 'clf'
convg_thres = 1e-5
max_epochs = 500
is_gpu = False
gpu_ids = 0

model = SupervisedAE(input_dim=input_dim, emb_dim=emb_dim, num_targets=num_targets, \
                    num_chunks=num_chunks, supervision=supervision, convg_thres=convg_thres, \
                    max_epochs=max_epochs, is_gpu=is_gpu, gpu_ids=gpu_ids)

#Hyperparameter search space
search_space = [Integer(1, 5, name='num_layers'),
                Real(10**-5, 10**-2, "log-uniform", name='learning_rate'),
                Real(10**-6, 10**-3, "log-uniform", name='weight_decay'),
                Categorical(['relu', 'sigma'], name='activation')]

@use_named_args(search_space)
def optimize(**params):
    print("Training with hyper-parameters: ", params)
    model.set_params(params)
    model.fit(X_train, y_train)
    return model.loss

# define the space of hyperparameters to search
result = gp_minimize(optimize, search_space, n_calls=10)
# summarizing finding:
print('best score: {}'.format(result.fun))
print('best params:')
print('num_layers: {}'.format(result.x[0]))
print('learning_rate: {}'.format(result.x[1]))
print('weight_decay: {}'.format(result.x[2]))
print('activation: {}'.format(result.x[3]))

num_layers = result.x[0]
learning_rate = result.x[1]
weight_decay = result.x[2]
activation = result.x[3]
model = SupervisedAE(input_dim=input_dim, emb_dim=emb_dim, num_targets=num_targets, \
                    num_layers=num_layers, learning_rate=learning_rate, weight_decay=weight_decay,
                    num_chunks=num_chunks, supervision=supervision, convg_thres=convg_thres, \
                    activation=activation, max_epochs=max_epochs, is_gpu=is_gpu, gpu_ids=gpu_ids)

model.fit(X_train, y_train)
model.save(path)

predicted,_ = model.predict(X_test)
predicted = torch.argmax(predicted, 1).numpy()

##Change according to the appropriate metrics function
print("Writing the metrics into file....")
f = open('metrics.txt', 'w+')
f.write(metrics.classification_report(y_test, predicted))
f.close()
print("Writing output into file....")
df = pd.DataFrame(data={'pred':predicted, 'true':y_test})
df.to_csv('output.csv', index=False)